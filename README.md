# RailWay Automation #

## Description ##

The Railway Automation is a selenium-based testing framework that aims to automate tests on Railway website.


### Prepare environment ###

* Clone the repo git clone https://tu-coder@bitbucket.org/tu-coder/ta_dashboard.git
* Install IntelliJ and Java

### Run the test ###

* Run test script on IntelliJ,

		Under Railway project, go to “src/test/resources/suites”
		Select the test suite file (RunRnegression.xml for example), right click on this file, select Run.

* Run test script via Maven,

		Go to Railway root folder on cmd window. 
		mvn clean test -DXML_FILE=<suite_file.xml> 
		i.e: mvn clean test -DXML_FILE=regression.xml

* Then go to Railway\Report\ folder, and then view result on file Test-Automaton-Report.html

### Read More ###