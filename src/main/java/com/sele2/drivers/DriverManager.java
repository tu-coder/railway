package com.sele2.drivers;

import com.sele2.utilities.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class DriverManager {
	WebDriver driver;
	WebDriverWait explicitWait;

	abstract void createDriver();

	public void quitDriver() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	public WebDriver getDriver() {
		if (driver == null) {
			createDriver();
			explicitWait = new WebDriverWait(driver, Constants.MEDIUM_TIMEOUT);
			driver.manage().timeouts().implicitlyWait(Constants.LONG_TIMEOUT, TimeUnit.SECONDS);
		}
		return driver;
	}


}
