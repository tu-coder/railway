package com.sele2.drivers;



public class DriverManagerFactory {
	protected static ThreadLocal<DriverManager> DRIVERS = new ThreadLocal<>();

	public static void setDriverManager(String browserName) {

		switch (browserName) {
		case "chrome.local":
			DRIVERS = ThreadLocal.withInitial(ChromeDriverManager::new);
			break;
		case "firefox.local":
			DRIVERS = ThreadLocal.withInitial(FirefoxDriverManager::new);
			break;
		}
	}
	
	public static DriverManager getDriverManager() {
		return DRIVERS.get();
	}


}
