package com.sele2.drivers;

import com.google.common.base.Function;
import com.sele2.helper.Log;
import com.sele2.utilities.Constants;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class DriverUtils extends DriverManagerFactory {

    public static WebDriver driver;
    public static long loadTimeout;
    static Set<String> allWindows;

	public static void switchToChildWindowByTitle(String title) {
		allWindows = getDriverManager().getDriver().getWindowHandles();
		for (String runWindow : allWindows) {
			getDriverManager().getDriver().switchTo().window(runWindow);
			String currentWin = getDriverManager().getDriver().getTitle();
			if (currentWin.equals(title)) {
				break;
			}

		}
	}

	public static void getBrowser(String browserName) {
		DriverManagerFactory.setDriverManager(browserName);
	}

	public static WebDriver getWebDriver() {
		return getDriverManager().getDriver();
	}

	public static void closeBrowserAndDriver() {
		try {
			String osName = System.getProperty("os.name").toLowerCase();
			Log.info("OS name = " + osName);

			String cmd = "";
			if (getDriverManager().getDriver() != null) {
				getDriverManager().getDriver().quit();
			}

			if (getDriverManager().getDriver().toString().toLowerCase().contains("chrome")) {
				if (osName.toLowerCase().contains("mac")) {
					cmd = "pkill chromedriver";
				} else if (osName.toLowerCase().contains("windows")) {
					cmd = "taskkill /F /FI \"IMAGENAME eq chromedriver*\"";
				}
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}

			if (getDriverManager().getDriver().toString().toLowerCase().contains("internetexplorer")) {
				if (osName.toLowerCase().contains("window")) {
					cmd = "taskkill /F /FI \"IMAGENAME eq IEDriverServer*\"";
					Process process = Runtime.getRuntime().exec(cmd);
					process.waitFor();
				}
			}
			Log.info("---------- QUIT BROWSER SUCCESS ----------");
		} catch (Exception e) {
			Log.info(e.getMessage());
		}
	}

	public static boolean closeAllWindowsWithoutParent(String parentWindow) {
		allWindows = getDriverManager().getDriver().getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parentWindow)) {
				getDriverManager().getDriver().switchTo().window(runWindow);
				getDriverManager().getDriver().close();
			}
		}
		getDriverManager().getDriver().switchTo().window(parentWindow);
		if (getDriverManager().getDriver().getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	public static void waitForPageLoad() {
		try {
			WebDriverWait wait = new WebDriverWait(getDriverManager().getDriver(), Constants.SHORT_TIMEOUT);

			wait.until(new Function<WebDriver, Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					Boolean domIsComplete = (Boolean) (executor
							.executeScript("return document.readyState == 'complete';"));
					return domIsComplete;
				}
			});
			Thread.sleep(1000);
		} catch (Exception e) {
		}
	}

	public static void openURL(String urlValue) {
		getDriverManager().getDriver().get(urlValue);
	}

	public static String getPageTitle() {
		return getDriverManager().getDriver().getTitle();
	}

	public static String getCurrentPageURL() {
		return getDriverManager().getDriver().getCurrentUrl();
	}

	public static String getPageSource() {
		return getDriverManager().getDriver().getPageSource();
	}

	public static void backToPage() {
		getDriverManager().getDriver().navigate().back();
	}

	public static void refreshCurrentPage() {
		getDriverManager().getDriver().navigate().refresh();
	}

	public static void fowardToPage() {
		getDriverManager().getDriver().navigate().forward();
	}

	public static void acceptAlert() {
		getDriverManager().getDriver().switchTo().alert().accept();
	}

	public static void cancelAlert() {
		getDriverManager().getDriver().switchTo().alert().dismiss();
	}

	public static String getTextAlert() {
		return getDriverManager().getDriver().switchTo().alert().getText();
	}

	public static void sendkeyToAlert(String value) {
		getDriverManager().getDriver().switchTo().alert().sendKeys(value);
	}

	public static void waitForAlertPresence() {
		WebDriverWait waitExplicit = new WebDriverWait(getDriverManager().getDriver(), Constants.LONG_TIMEOUT);
		waitExplicit.until(ExpectedConditions.alertIsPresent());
	}

}
