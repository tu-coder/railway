package com.sele2.elements;

import org.openqa.selenium.By;

public class Button extends Element {

	public Button(By byLocator) {
		super(byLocator);
	}

	public Button(String locator, Object... args) {
		super(locator, args);
	}

	public Button(String locator) {
		super(locator);
	}

}
