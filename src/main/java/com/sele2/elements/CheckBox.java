package com.sele2.elements;

import com.sele2.utilities.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class CheckBox extends Element {

	public CheckBox(By byLocator) {
		super(byLocator);
	}

	public CheckBox(String locator, Object... args) {
		super(locator, args);
	}

	public CheckBox(String locator) {
		super(locator);
	}
	
	public boolean isChecked() {
		return isSelected();
	}

	public void check() {
		if (!isChecked()) {
			click();
		}
	}
	
	public void uncheck() {
		if (isChecked()) {
			click();
		}
	}
	
	public void set(boolean value) {
		if (value) {
			check();
		} else {
			uncheck();
		}
	}
	
	public void waitForCheckBoxIsChecked() {
		List<WebElement> elements = getElements();
		for (WebElement element:elements) {
			waitExplicit = new WebDriverWait(getDriver(), Constants.LONG_TIMEOUT);
			waitExplicit.until(ExpectedConditions.elementToBeSelected(element));
		}
	}

}
