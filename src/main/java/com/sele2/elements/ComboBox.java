package com.sele2.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class ComboBox extends Element{

	public ComboBox(By byLocator) {
		super(byLocator);
		
	}

	public ComboBox(String locator, Object... args) {
		super(locator, args);
	
	}

	public ComboBox(String locator) {
		super(locator);
	
	}
	
	public void select(String text) {
		Select select = new Select(getElement());
		select.selectByVisibleText(text);

	}
	
	public void select(int index) {
		Select select = new Select(getElement());
		select.selectByIndex(index);

	}
	
	public String getSelected() {
		Select select = new Select(getElement());
		return select.getFirstSelectedOption().getText();
	}

	public List<String> getOptions() {
		List<String> ops = new ArrayList<String>();
		Select select = new Select(getElement());
		List<WebElement> options = select.getOptions();
		for (WebElement option : options) {
			ops.add(option.getText());
		}
		return ops;
	}

	

}
