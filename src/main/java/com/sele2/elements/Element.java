package com.sele2.elements;

import com.sele2.utilities.Constants;
import com.sele2.drivers.DriverUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Element {

	String locator;
	By byLocator;
	String dynamicLocator;
	public WebDriverWait waitExplicit;
	Actions action;
	private Log log = LogFactory.getLog(getClass());

	public Element(String locator) {
		this.locator = locator;
		this.dynamicLocator = locator;
		this.byLocator = getByLocator();
	}

	public Element(By byLocator) {
		this.byLocator = byLocator;
	}

	public Element(String locator, Object... args) {
		this.dynamicLocator = locator;
		this.locator = String.format(dynamicLocator, args);
		this.byLocator = getByLocator();
	}

	private By getByLocator() {
		String body = this.locator.replaceAll("[\\w\\s]*=(.*)", "$1").trim();
		String type = this.locator.replaceAll("([\\w\\s]*)=.*", "$1").trim();
		switch (type) {
		case "css":
			return By.cssSelector(body);
		case "id":
			return By.id(body);
		case "link":
			return By.linkText(body);
		case "xpath":
			return By.xpath(body);
		case "text":
			return By.xpath(String.format("//*[contains(text(), '%s')]", body));
		case "name":
			return By.name(body);
		default:
			return By.xpath(locator);
		}
	}

	public WebDriver getDriver() {
		return DriverUtils.getWebDriver();
	}

	public WebElement getElement() {
		return getDriver().findElement(getLocator());
	}

	public List<WebElement> getElements() {
		return getDriver().findElements(getLocator());
	}

	protected JavascriptExecutor jsExecutor() {
		return (JavascriptExecutor) getDriver();
	}

	public By getLocator() {
		return this.byLocator;
	}

	public void setDynamicValue(Object... args) {
		this.locator = String.format(this.dynamicLocator, args);
		this.byLocator = getByLocator();
	}

	public void overrideGlobalTimeout(long shortTimeout) {
		getDriver().manage().timeouts().implicitlyWait(shortTimeout, TimeUnit.SECONDS);
	}

	public void waitForVisible() {
		waitExplicit = new WebDriverWait(getDriver(), Constants.LONG_TIMEOUT);
		try {
			waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(getLocator()));
		} catch (Exception ex) {
			log.debug("Element doesn't exist");
		}
	}

	public void waitForInvisible() {
		waitExplicit = new WebDriverWait(getDriver(), Constants.LONG_TIMEOUT);
		overrideGlobalTimeout(Constants.SHORT_TIMEOUT);
		waitExplicit.until(ExpectedConditions.invisibilityOfElementLocated(getLocator()));
		overrideGlobalTimeout(Constants.LONG_TIMEOUT);
	}

	public void waitForDisplay() {
		waitExplicit = new WebDriverWait(getDriver(), Constants.LONG_TIMEOUT);
		try {
			waitExplicit.until(ExpectedConditions.presenceOfElementLocated(getLocator()));
		} catch (Exception ex) {
			log.debug("Element doesn't display");
		}
	}

	public void waitForClickable() {
		waitExplicit = new WebDriverWait(getDriver(), Constants.LONG_TIMEOUT);
		try {
			waitExplicit.until(ExpectedConditions.elementToBeClickable(getLocator()));
		} catch (Exception ex) {
			log.debug("Element doesn't exist");
		}
	}

	public boolean isDisplay() {
		boolean status = true;
		try {
			if (getElement().isDisplayed()) {
				return status;
			}
		} catch (Exception ex) {
			status = false;
		}
		return status;
	}

	public boolean isUndisplayed() {
		overrideGlobalTimeout(Constants.SHORT_TIMEOUT);
		if (getElements().size() == 0) {
			System.out.println("Element not in DOM");
			overrideGlobalTimeout(Constants.LONG_TIMEOUT);
			return true;
		} else if (getElements().size() > 0 && !getElements().get(0).isDisplayed()) {
			System.out.println("Element in DOM but not visible/display");
			overrideGlobalTimeout(Constants.LONG_TIMEOUT);
			return true;
		} else {
			System.out.println("Element in DOM and visible");
			overrideGlobalTimeout(Constants.LONG_TIMEOUT);
			return false;
		}
	}

	public boolean isClickable() {
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(), Constants.LONG_TIMEOUT);
			wait.until(ExpectedConditions.elementToBeClickable(getLocator()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isEnabled() {
		boolean status = true;
		try {
			if (getElement().isEnabled()) {
				return status;
			}
		} catch (Exception ex) {
			status = false;
		}
		return status;
	}

	public boolean isDisabled() {
		boolean status = true;
		try {
			getElement().getAttribute("disabled");
			return status;
		} catch (Exception ex) {
			status = false;
		}
		return status;
	}

	public boolean isSelected() {
		boolean status = true;
		try {
			if (getElement().isSelected()) {
				return status;
			}
		} catch (Exception ex) {
			status = false;
		}
		return status;
	}

	public String getText() {
		waitForVisible();
		return getElement().getText();
	}

	public String getValue() {
		waitForVisible();
		return getElement().getAttribute("value").trim();
	}

	public String getAttribue(String attributeName) {
		waitForVisible();
		return getElement().getAttribute(attributeName);
	}

	public void click() {
		getElement().click();
	}

	public void enter(String value) {
		getElement().sendKeys(value);
	}

	public Object executeSriptJS(String javaSript) {
		return jsExecutor().executeScript(javaSript, getElement());
	}

	public void clickByJS() {
		jsExecutor().executeScript("arguments[0].click();", getElement());
	}

	public void scrollToView() {
		jsExecutor().executeScript("arguments[0].scrollIntoView(true);", getElement());
	}

	public void sendkeyToElementByJS(String value) {
		jsExecutor().executeScript("arguments[0].setAttribute('value', '" + value + "')", getElement());
	}

	public void hover() {
		action = new Actions(getDriver());
		action.moveToElement(getElement()).perform();
	}

	public void doubleClick() {
		action = new Actions(getDriver());
		action.doubleClick(getElement()).perform();
	}

	public void rightClick() {
		action = new Actions(getDriver());
		action.contextClick(getElement()).perform();
	}

	public void dragAndDrop(String sourceLocator, String targetLocator) {
		action = new Actions(getDriver());
		WebElement sourcElement = getDriver().findElement(By.xpath(sourceLocator));
		WebElement targetElement = getDriver().findElement(By.xpath(targetLocator));
		action.dragAndDrop(sourcElement, targetElement).perform();
	}

	public void sendKeyboardToElement(Keys key) {
		action = new Actions(getDriver());
		action.sendKeys(getElement(), key).perform();
	}
	
	public void clear() {
		getElement().clear();
	}

}
