package com.sele2.elements;

import org.openqa.selenium.By;

public class Frame extends Element{

	public Frame(By byLocator) {
		super(byLocator);
	
	}

	public Frame(String locator, Object... args) {
		super(locator, args);
		
	}

	public Frame(String locator) {
		super(locator);
		
	}
	
	public void switchTo() {
		getDriver().switchTo().frame(getElement());
	}
	
}
