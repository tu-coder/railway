package com.sele2.elements;

import org.openqa.selenium.By;

public class Label extends Element{

	public Label(By byLocator) {
		super(byLocator);
	}

	public Label(String locator, Object... args) {
		super(locator, args);
	}

	public Label(String locator) {
		super(locator);
	}

}
