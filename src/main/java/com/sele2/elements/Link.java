package com.sele2.elements;

import org.openqa.selenium.By;

public class Link extends Element {

	public Link(By byLocator) {
		super(byLocator);
	}

	public Link(String locator, Object... args) {
		super(locator, args);
	}

	public Link(String locator) {
		super(locator);
	}

}
