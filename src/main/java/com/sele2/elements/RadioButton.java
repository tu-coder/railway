package com.sele2.elements;

import org.openqa.selenium.By;

public class RadioButton extends Element {

	public RadioButton(By byLocator) {
		super(byLocator);
	}

	public RadioButton(String locator, Object... args) {
		super(locator, args);
	}

	public RadioButton(String locator) {
		super(locator);
	}

	public boolean isChecked() {
		return isSelected();
	}

	public void check() {
		if (!isChecked()) {
			click();
		}
	}
	
	public void uncheck() {
		if (isChecked()) {
			click();
		}
	}
	
	public void set(boolean value) {
		if (value) {
			check();
		} else {
			uncheck();
		}
	}
}
