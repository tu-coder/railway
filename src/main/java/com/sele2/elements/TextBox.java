package com.sele2.elements;

import org.openqa.selenium.By;

public class TextBox extends Element {

	public TextBox(By byLocator) {
		super(byLocator);

	}

	public TextBox(String locator, Object... args) {
		super(locator, args);

	}

	public TextBox(String locator) {
		super(locator);

	}

}
