package com.sele2.helper;

import org.apache.commons.lang3.RandomStringUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringHelper {
    public static boolean isRegexMatch(String text, String regex) {
        if (text == null)
            return false;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static List<String> extractStringAsList(String text, String regex) {
        List<String> result = new ArrayList<>();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            result.add(matcher.group());
        }
        return result;
    }

    public static String getRandomEmail() {
        SimpleDateFormat sdTimeStr;
        sdTimeStr = new SimpleDateFormat("ddMMhhmmss");
        String newName= "ngoctu" + sdTimeStr.format(new Date())+"@mailinator.com";
        return  newName;
    }

    public static String generateRandomString(int lengh, String str){
        String allowedChars="abcdefghijklmnopqrstuvwxyz" + "1234567890";
        String randomString = RandomStringUtils.random(lengh, allowedChars);
        return str + randomString;
    }
}
