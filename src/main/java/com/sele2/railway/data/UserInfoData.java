package com.sele2.railway.data;

public class UserInfoData extends DataBase {

	public UserInfoData(String jsonFile) {
		super(jsonFile);
	}

	public String getInvalidUsername() {
		return invalidUsername;
	}

	public void setInvalidUsername(String invalidUsername) {
		this.invalidUsername = invalidUsername;
	}

	public String getInvalidPassword() {
		return invalidPassword;
	}

	public void setInvalidPassword(String invalidPassword) {
		this.invalidPassword = invalidPassword;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDepartDate() {
		return departDate;
	}

	public void setDepartDate(String departDate) {
		this.departDate = departDate;
	}

	public String getDepartFrom() {
		return departFrom;
	}

	public void setDepartFrom(String departFrom) {
		this.departFrom = departFrom;
	}

	public String getArriveAt() {
		return arriveAt;
	}

	public void setArriveAt(String arriveAt) {
		this.arriveAt = arriveAt;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = departFrom;
	}

	public String getTicketAmount() {
		return ticketAmount;
	}

	public void setTicketAmount(String ticketAmount) {
		this.ticketAmount = ticketAmount;
	}

	private String username;
	private String password;
	private String invalidUsername;
	private String invalidPassword;
	private String departDate;
	private String departFrom;
	private String arriveAt;
	private String seatType;
	private String ticketAmount;
}
