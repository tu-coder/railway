package com.sele2.railway.pages;

import com.sele2.elements.Button;
import com.sele2.elements.Label;
import com.sele2.elements.Element;
import com.sele2.elements.ComboBox;


public class BookTicketPage extends GeneralPage{
    ComboBox departDateList = new ComboBox("//select[@name='Date']");
    ComboBox departFromList = new ComboBox("//select[@name='DepartStation']");
    ComboBox arriveAtList = new ComboBox("//select[@name='ArriveStation']");
    ComboBox seatTypeList = new ComboBox("//select[@name='SeatType']");
    ComboBox ticketAmountList = new ComboBox("//select[@name='TicketAmount']");
    Button bookTicketBtn = new Button("//input[@value='Book ticket']");
    Label departStation = new Label ("//tr[1][@th='Depart Station']");
    Label arriveStation = new Label("//tr[1][@th='Arrive Station']");
    Label successMessage = new Label("//*[@id='content']/h1");
    private static final Label lblPid = new Label("//li[2]/strong[2]");
    String bookedTicket = "//td[text()='%s']/following-sibling::td[text()='%s']/following-sibling::td[text()='%s']/following-sibling::td[text()='%s']//following-sibling::td[text()='%s']";


    public void bookTicket(String departDate, String departFrom, String arriveAt, String seatType, String ticketAmount){
        departDateList.select(departDate);
        departFromList.select(departFrom);
        arriveAtList.select(arriveAt);
        seatTypeList.select(seatType);
        ticketAmountList.select(ticketAmount);
        scrollBottom();
        bookTicketBtn.click();
    }

    public void bookTickets(int numberOfTicket){
        for (int i = 1; i <= numberOfTicket; i++) {
            scrollBottom();
            bookTicketBtn.click();
            this.goToBookTicketPage();
        }
    }

    public String getBookTicketSuccessfullyMessage(){
        return successMessage.getText();
    }

    public String getDepartStation(){
        return departStation.getText();
    }

    public String getArriveStation(){
        return arriveStation.getText();
    }

    public String getTextPid() {
        return lblPid.getText();
    }

    public boolean isBookedTicketDisplayed(String departFrom, String arriveAt, String seatType,String departDate,String ticketAmount) {
            String str = String.format(bookedTicket,departFrom,arriveAt,seatType,departDate,ticketAmount);
            Element ticket = new Element(str);
            return ticket.isDisplay();
    }
}
