package com.sele2.railway.pages;

import com.sele2.elements.Label;
import com.sele2.elements.Link;
import com.sele2.utilities.PageName;
import org.openqa.selenium.By;


public class GeneralPage {
    Label headerText = new Label("//*[@id='content']/h1");
    String welcomeMessage = "//*[@id='banner']/div/strong]";
    Link bottomLink = new Link("//a[.='Web hosting by Somee.com']");
    Link dynamicXpathTab = new Link("//a[.='%s']");

    public String getPageHeader () {
        return headerText.getText();
    }

    public void scrollBottom() {
        bottomLink.scrollToView();
    }

    public void goToLoginPage(){
        dynamicXpathTab.setDynamicValue(PageName.LOGIN);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToRegisterPage(){
        dynamicXpathTab.setDynamicValue(PageName.REGISTER);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToBookTicketPage(){
        dynamicXpathTab.setDynamicValue(PageName.BOOK_TICKET);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToMyTicketPage(){
        dynamicXpathTab.setDynamicValue(PageName.MY_TICKET);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToHomePage(){
        dynamicXpathTab.setDynamicValue(PageName.HOME);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToLogoutTab(){
        dynamicXpathTab.setDynamicValue(PageName.LOGOUT);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToFAQPage(){
        dynamicXpathTab.setDynamicValue(PageName.FAQ);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToTimetablePage(){
        dynamicXpathTab.setDynamicValue(PageName.TIMETABLE);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToContactPage(){
        dynamicXpathTab.setDynamicValue(PageName.CONTACT);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }
    public void goToTicketPricePage(){
        dynamicXpathTab.setDynamicValue(PageName.TICKET_PRICE);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }

    public void goToChangePasswordPage(){
        dynamicXpathTab.setDynamicValue(PageName.CHANGE_PASSWORD);
        dynamicXpathTab.waitForVisible();
        dynamicXpathTab.click();
    }
}
