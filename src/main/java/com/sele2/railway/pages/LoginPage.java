package com.sele2.railway.pages;

import com.sele2.elements.Button;
import com.sele2.elements.Label;
import com.sele2.elements.TextBox;
import com.sele2.elements.Link;

public class LoginPage extends GeneralPage{
    TextBox txtEmail = new TextBox("id=username");
    TextBox txtPassword = new TextBox("id=password");
    Button btnLogIn = new Button("//input[@value='login']");
    Label lblWelcomeMessage = new Label("//p[@class='message error LoginForm']");
    Link registrationLink = new Link ("//*[@id='content']/ul/li[2]/a");

    public void login(String username, String password){
        txtEmail.enter(username);
        txtPassword.enter(password);
        btnLogIn.click();
    }
    public String getErrorMessage ()
    {
        return this.lblWelcomeMessage.getText();
    }

    public void clickRegistrationLink(){
        registrationLink.click();
    }

}
