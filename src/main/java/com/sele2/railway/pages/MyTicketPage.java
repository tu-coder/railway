package com.sele2.railway.pages;

import com.sele2.elements.Element;
import com.sele2.elements.Alert;
import com.sele2.elements.Button;
import com.sele2.helper.StringHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class MyTicketPage extends GeneralPage{
    public static final Element txtNote = new Element("//*[@id='content']/div/li[1]");
    public static final Element txtMessageNoData = new Element("//*[@id='content']");
    private final String btnCancel = "//td[text()='%s']/following-sibling::td[text()='%s']/following-sibling::td[text()='%s']/..//td//input[@value='Cancel']";
    Alert alert = new Alert();

    public boolean isDisplayMessageNoData() {
        boolean result = false;
        if (txtNote.isDisplay()) {
            result = getAmountOfTicketOnNote() == 0;
        } else {
            String txt = txtMessageNoData.getText();
            result = txt.contains("You haven't booked any tickets yet.");
        }
        return false;
    }

    public int getAmountOfTicketOnNote() {
        if (txtNote.isDisplay()) {
            String txt = txtNote.getText();
            List<String> numList = StringHelper.extractStringAsList(txt, "\\d+");
            return Integer.parseInt(numList.get(0));
        }
        return 0;
    }
    public void cancelTicket(String departStation, String arriveStation, String seatType) {
        scrollBottom();
        String str = String.format(btnCancel, departStation,arriveStation,seatType);
        Element button = new Element(str);
        while (!isDisplayMessageNoData()) {
            button.click();
            alert.waitForAlertPresent();
            alert.accept();
        }
    }

}
