package com.sele2.railway.pages;

import com.sele2.elements.Label;
import com.sele2.elements.Button;
import com.sele2.elements.TextBox;

public class RegisterPage extends GeneralPage{
    TextBox emailTextBox = new TextBox("id = email");
    TextBox passwordTextBox = new TextBox("id=password");
    TextBox confirmPasswordTextBox = new TextBox("id=confirmPassword");
    TextBox passportTextBox = new TextBox("id=pid");
    Button registerButton = new Button("//input[@value='Register']");
    Label messageSuccess = new Label("TagName = p");
    Label errorMessage = new Label("//p[@class='message error']");


    public void registerNewAccount(String email, String password, String confirmPassword, String pidNumber) {
        emailTextBox.enter(email);
        passwordTextBox.enter(password);
        confirmPasswordTextBox.enter(confirmPassword);
        passportTextBox.enter(pidNumber);
        registerButton.click();
    }

    public String getMessageSuccess() {
        messageSuccess.waitForVisible();
        return messageSuccess.getText();
    }

    public String getErrorMessage() {
        errorMessage.waitForVisible();
        return errorMessage.getText();
    }

}
