package com.sele2.utilities;


public class Constants {
    public static final String DATA_PATH = "src/test/resources/data/";
    public static final String RAILWAY_URL = "http://www.railway.somee.com/Page/HomePage.cshtml";
    public static final String EMAIL_1 = "april.mango.44@gmail.com";
    public static final String PASSWORD = "12345678";
    public static final String EMAIL_2 = "april.mango.45@gmail.com";
    public static final String INVALID_PASSWORD = "abc12345";
    public static final String PID_NUMBER = "12345678";
    public static final long SHORT_TIMEOUT = 5;
    public static final long MEDIUM_TIMEOUT = 20;
    public static final long LONG_TIMEOUT = 30;
    public static final String USER_DIR = System.getProperty("user.dir");
    public static final String FileSeparator = System.getProperty("file.separator");

}
