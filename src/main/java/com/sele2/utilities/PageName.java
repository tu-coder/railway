package com.sele2.utilities;

public class PageName {
    public static final String HOME = "Home";
    public static final String FAQ = "FAQ";
    public static final String CONTACT = "Contact";
    public static final String TIMETABLE = "Timetable";
    public static final String TICKET_PRICE = "Ticket price";
    public static final String BOOK_TICKET = "Book ticket";
    public static final String MY_TICKET = "My ticket";
    public static final String CHANGE_PASSWORD = "Change password";
    public static final String LOGIN = "Login";
    public static final String LOGOUT = "Log out";
    public static final String REGISTER = "Register";
}
