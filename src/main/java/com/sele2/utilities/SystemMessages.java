package com.sele2.utilities;

public class SystemMessages {
    public static final String ERROR_LOGIN_MESSAGE = "There was a problem with your login and/or errors exist in your form.";
    public static final String SUCCESS_REGISTER_MESSAGE = "Thank you for registering your account";
    public static final String REGISTRATION_CONFIRMED_MESSAGE = "Registration Confirmed! You can now log in to the site.";
    public static final String EXISTED_EMAIL_MESSAGE = "This email address is already in use.";
    public static final String ERROR_REGISTER_MESSAGE = "There're errors in the form. Please correct the errors and try again.";
    public static final String BOOK_TICKET_SUCCESS_MESSAGE = "Ticket booked successfully!";
}
