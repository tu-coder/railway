package com.sele2.utilities;

import com.sele2.drivers.DriverUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Calendar;
import java.util.TimeZone;

public class Utils {

    // Wait for page load
    public static void waitForPageLoad() {
        Wait<WebDriver> wait = new WebDriverWait(DriverUtils.driver,DriverUtils.loadTimeout);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver wdriver) {
                return ((JavascriptExecutor) DriverUtils.driver)
                        .executeScript("return document.readyState").equals("complete");
            }
        });
    }
    public static void waitForPageStable() {
        Wait<WebDriver> wait = new WebDriverWait(DriverUtils.driver,DriverUtils.loadTimeout);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return (Boolean) ((JavascriptExecutor) driver)
                        .executeScript("return (window.jQuery != null) && (jQuery.active === 0);");
            }
        });
    }

    public static String getDate(Calendar cal){
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        return "" + (cal.get(Calendar.MONTH)+1) +"/" +
                cal.get(Calendar.DATE) + "/" + cal.get(Calendar.YEAR);
    }
    public static String getThreeDaysAfterCurrentDate(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 3);
        return getDate(cal);
    }
}
