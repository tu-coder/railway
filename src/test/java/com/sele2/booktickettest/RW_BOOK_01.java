package com.sele2.booktickettest;

import com.sele2.railway.data.UserInfoData;
import com.sele2.railway.pages.HomePage;
import com.sele2.railway.pages.LoginPage;
import com.sele2.railway.pages.BookTicketPage;;
import com.sele2.reports.Reporter;
import com.sele2.testbase.TestBase;
import com.sele2.utilities.Constants;
import com.sele2.utilities.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;


public class RW_BOOK_01 extends TestBase {

    @Test(description = "Book_Ticket_01: Verify that the 'Ticket booked successfully!' page displays correct information")
    public void RW_BOOK_01(){
        tcData = new UserInfoData(Constants.DATA_PATH + "RW_BOOK_01.json");
        tcData = tcData.getData();

        Reporter.log("Step 1: Go to Login page");
        homePage.goToLoginPage();

        Reporter.log("Step 2: Log in with valid account");
        loginPage.login(Constants.EMAIL_1, Constants.PASSWORD);

        Reporter.log("Step 3: Go to Book Ticket page");
        homePage.goToBookTicketPage();

        Reporter.log("Step 4: Book 1 ticket");
        bookTicketPage.bookTicket(Utils.getThreeDaysAfterCurrentDate(),
                tcData.getDepartFrom(),
                tcData.getArriveAt(),
                tcData.getSeatType(),
                tcData.getTicketAmount());

        Reporter.log("VP: Verify the ticket information matches with the booking data");
        Assert.assertTrue(bookTicketPage.isBookedTicketDisplayed(tcData.getDepartFrom(),
                tcData.getArriveAt(),
                tcData.getSeatType(),Utils.getThreeDaysAfterCurrentDate(),
                tcData.getTicketAmount()));

        Reporter.log("VP: Verify the PID/Passport number must be the one used for registered");
        Assert.assertEquals(bookTicketPage.getTextPid(), Constants.PID_NUMBER);
    }
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    BookTicketPage bookTicketPage = new BookTicketPage();
    UserInfoData tcData;
}
