package com.sele2.logintest;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.sele2.railway.pages.HomePage;
import com.sele2.railway.pages.LoginPage;
import com.sele2.testbase.TestBase;
import com.sele2.reports.Reporter;


public class RW_LOGIN_01 extends TestBase{
        @Test(description = "Log_01: Verify that user can open the Login page")
        public void RW_LOGIN_01(){
                Reporter.log("Step 1: Click on Login tab");
                homePage.goToLoginPage();
                Reporter.log("VP: Verify Login page is displayed");
                Assert.assertEquals(loginPage.getPageHeader(), "Login page");
        }
        HomePage homePage = new HomePage();
        LoginPage loginPage = new LoginPage();
}
