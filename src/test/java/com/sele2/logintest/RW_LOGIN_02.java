package com.sele2.logintest;


import com.sele2.railway.pages.HomePage;
import com.sele2.railway.pages.LoginPage;
import com.sele2.railway.pages.RegisterPage;
import com.sele2.reports.Reporter;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.sele2.testbase.TestBase;

public class RW_LOGIN_02 extends TestBase{

    @Test(description = "Log_02: Verify that clicking on the hyperlink text registration page will redirect to the Register page")
    public void RW_LOGIN_02(){
        Reporter.log("Step 1: Go to Login page");
        homePage.goToLoginPage();

        Reporter.log("Step 2: Click on Register link");
        loginPage.clickRegistrationLink();

        Reporter.log("VP: Verify Register page is displayed");
        Assert.assertEquals(registerPage.getPageHeader(), "Create account");
    }
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    RegisterPage registerPage = new RegisterPage();
}
