package com.sele2.mytickettest;


import com.sele2.helper.StringHelper;
import com.sele2.railway.data.UserInfoData;
import com.sele2.railway.pages.*;
import com.sele2.reports.Reporter;
import com.sele2.testbase.TestBase;
import com.sele2.utilities.Constants;
import com.sele2.utilities.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;


public class RW_TICKET_02 extends TestBase {

    @Test(description = "Ticket_02: Verify that booking newly ticket does not make old booked ticket disappear")
    public void RW_TICKET_02(){
        /*Register a new account*/
        String email = StringHelper.getRandomEmail();
        homePage.goToRegisterPage();
        registerPage.registerNewAccount(email, Constants.PASSWORD,Constants.PASSWORD,Constants.PID_NUMBER);

        tcData = new UserInfoData(Constants.DATA_PATH + "RW_TICKET_02.json");
        tcData = tcData.getData();

        Reporter.log("Step 1: Go to Login page");
        homePage.goToLoginPage();

        Reporter.log("Step 2: Log in with valid account");
        loginPage.login(email,Constants.PASSWORD);

        Reporter.log("Step 3: Go to Book Ticket Page");
        homePage.goToBookTicketPage();

        Reporter.log("Step 4: Book 1 new ticket");
        bookTicketPage.bookTicket(Utils.getThreeDaysAfterCurrentDate(),
                tcData.getDepartFrom(),
                tcData.getArriveAt(),
                tcData.getSeatType(),
                tcData.getTicketAmount());

        Reporter.log("Step 5: Go to Book Ticket page");
        myTicketPage.goToBookTicketPage();

        Reporter.log("Step 6: Book 9 new tickets");
        bookTicketPage.bookTickets(9);

        Reporter.log("Step 7: Go to My Ticket page");
        bookTicketPage.goToMyTicketPage();

        Reporter.log("VP: Verify total new tickets with the message in the notes");
        Assert.assertEquals(10,myTicketPage.getAmountOfTicketOnNote());

    }
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    RegisterPage registerPage = new RegisterPage();
    BookTicketPage bookTicketPage = new BookTicketPage();
    MyTicketPage myTicketPage = new MyTicketPage();
    UserInfoData tcData;

}
