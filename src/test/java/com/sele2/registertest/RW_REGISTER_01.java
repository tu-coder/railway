package com.sele2.registertest;

import com.sele2.railway.pages.HomePage;
import com.sele2.railway.pages.RegisterPage;
import com.sele2.reports.Reporter;
import com.sele2.testbase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RW_REGISTER_01 extends TestBase {

    @Test(description = "Reg_01: Verify that user can open the Register page")
    public void RW_REGISTER_01(){

        Reporter.log("Step 1: Click on Register tab");
        homePage.goToRegisterPage();

        Reporter.log("VP: Verify Register page is displayed");
        Assert.assertEquals(registerPage.getPageHeader(), "Create account");
    }
    HomePage homePage = new HomePage();
    RegisterPage registerPage = new RegisterPage();
}
