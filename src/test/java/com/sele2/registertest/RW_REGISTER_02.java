package com.sele2.registertest;

import com.sele2.railway.pages.HomePage;
import com.sele2.railway.pages.RegisterPage;
import com.sele2.utilities.Constants;
import com.sele2.testbase.TestBase;
import com.sele2.reports.Reporter;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.sele2.utilities.SystemMessages;

public class RW_REGISTER_02 extends TestBase{

    @Test(description = "Reg_02: Verify that user cannot create new account using Email that has been registered")
    public void RW_REGISTER_02(){
        Reporter.log("Step 1: Go to Register page");
        homePage.goToRegisterPage();

        Reporter.log("Step 2: Register a new account using an Email that has been registered");
        registerPage.registerNewAccount(Constants.EMAIL_1, Constants.PASSWORD, Constants.PASSWORD, Constants.PID_NUMBER);

        Reporter.log("VP: Verify Error for existing account is displayed");
        Assert.assertEquals(registerPage.getErrorMessage(), SystemMessages.EXISTED_EMAIL_MESSAGE);

    }
    HomePage homePage = new HomePage();
    RegisterPage registerPage = new RegisterPage();
}
