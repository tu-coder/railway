package com.sele2.testbase;

import com.sele2.reports.ExtentManager;
import com.sele2.reports.ExtentTestManager;
import com.sele2.utilities.Constants;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import com.sele2.drivers.DriverUtils;


public class TestBase  {
    @Parameters("browser")
    @BeforeMethod
    public void initTest(String browserName) {
        DriverUtils.getBrowser(browserName);
        DriverUtils.openURL(Constants.RAILWAY_URL);
    }

    @AfterClass
    public void afterMethod() {
        DriverUtils.closeBrowserAndDriver();
        ExtentTestManager.endTest();
        ExtentManager.getInstance().flush();
    }
}
